/*
* @Author: wangshudong
* @Date:   2017-05-22 15:55:06
* @Last Modified by:   wangshudong
* @Last Modified time: 2017-05-22 15:57:40
*/

'use strict';
require('slider.js');

// import shudong from 'shudong-test'
// console.log(shudong);

var slider = new MasterSlider();
exports.slider = slider;
